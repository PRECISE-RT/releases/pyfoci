import pyfoci

damage = 'example_files/DSBMarker.out'
microscope_json = 'pyfoci/microscopes/testscope.json'
time_to_view = 0
print("Reading file - DSB Marker.....")
microscope_object = pyfoci.microscope.Microscope(damage, microscope_json, cell_radius=12)
print("Defining Microscope.....")
microscope_object.define_microscope()
print("Mapping Breaks.....")
microscope_object.mapping_breaks(degrees_rotated=0)
print("Creating Microscope World.....")
world = microscope_object.create_world()
print("Placing Breaks.....")
broken_world = microscope_object.place_breaks(world, time_to_view, z_stack_offset=0)
print("Getting max value for normalisation.....")
microscope_object.max_value_world()
print("Making Images.....")
view_object = pyfoci.view.View(microscope_object, broken_world)
view_object.viewer()
print("Showing Image - close window to continue.....")
view_object.display_microscope_image()

damage = 'example_files/H2AXMarker.npz'
microscope_json = 'pyfoci/microscopes/testscope.json'
time_to_view = 0
print("Reading file - DSB Marker.....")
microscope_object = pyfoci.microscope.Microscope(damage, microscope_json, cell_radius=12)
print("Defining Microscope.....")
microscope_object.define_microscope()
print("Mapping Breaks.....")
microscope_object.mapping_breaks(degrees_rotated=0)
print("Creating Microscope World.....")
world = microscope_object.create_world()
print("Placing Breaks.....")
broken_world = microscope_object.place_breaks(world, time_to_view, z_stack_offset=0)
print("Getting max value for normalisation.....")
microscope_object.max_value_world()
print("Making Images.....")
view_object = pyfoci.view.View(microscope_object, broken_world)
view_object.viewer()
print("Showing Image - close window to continue.....")
view_object.display_microscope_image()