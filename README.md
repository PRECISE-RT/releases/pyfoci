# PyFoci

A computational approach to visualising and evaluating radiation-induced fluorescent foci with Python.

## Getting Started

Pyfoci is currently a toolset to allow for the quick and simple creation of microscope images. To get started quickly follow these steps:

1. Make a python environment to run the microscope in. Always recommend the use of virtual environments for safety. Must be Python 3.6 and above.

2. Install requirement.txt file into your new python environment
```
pip install -r requirements.txt
```

3. Test install using provided script
```
python demo.py
```

### Packages used

The following packages will be installed when using requirements.txt
* [h5py](http://www.h5py.org/)
* [matplotlib](https://matplotlib.org/)
* [numpy](https://numpy.org/)
* [Pillow](https://pillow.readthedocs.io/en/stable/)
* [plotly](https://plotly.com/)
* [scipy](https://www.scipy.org/)
* [numba](http://numba.pydata.org/)
* [scikit-image](https://scikit-image.org/)

## Example Use:

### Scripts
The following scripts have been created for easy use of the Pyfoci toolset. All scripts are suitable to be called through consoles, making them well suited for extensive Pyfoci image generation.

Save created figure:
```
python pyfoci-save.py -d <damagefile> -m <microscopefile> -t <viewtime> -z <z-slice> -s <save_name>
```

View created figure:
```
python pyfoci-view.py -d <damagefile> -m <microscopefile> -t <viewtime> -z <z-slice>
```

An example damage file and microscope file are example_files/DSBMarker.out and pyfoci/microscopes/testscope.json respectively.

If you have any MemoryError then try reducing the FOV magnification value in the Microscope JSON file.

## Contributing

We welcome anyone wishing to expand the functionality of the G-NOME project. Therefore, if you wish to contribute to the project please see our [contributing guidelines](https://gitlab.com/PRECISE-RT/releases/pyfoci/-/blob/master/CONTRIBUTING.md).

If you are interested in contributing to our collection of microscope PSFs please [let us know](mailto:samuel.ingram@postgrad.manchester.ac.uk?subject=PyFoci%20Microscope%20PSF%20Contibution) and we will be happy to assist.

## Developers

* **Samuel Ingram** - *Main Developer* - [SamPIngram](https://github.com/SamPIngram?tab=repositories)

## License

This package is licensed open-source under the GNU GENERAL PUBLIC LICENSE Version 3.

## Acknowledgments

* This project was started at the Computing Radiation Effects for Advancing Medicine (CREAM-1) workshop 3rd-6th April 2018, Queen's University, Belfast
* This work is part of a collaborative effort between the University of Manchester, Queen's University and Massachusetts General Hospital & Harvard Medical School

## Cite
If you use this software in your work it would be appreciated if you could cite the following:

Ingram et al., A computational approach to quantifying miscounting of radiation-induced double-strand break immunofluorescent foci. Commun Biology (2022).
