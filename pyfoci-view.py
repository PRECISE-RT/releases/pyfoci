import pyfoci
import sys
import getopt


def main(argv):

    damage, microscope_json, time_to_view, z_position, save_name, output_dir, user_output_dir = \
        "", "", 0, 0, "", "", False

    try:
        opts, args = getopt.getopt(argv, "hd:m:t:z:",
                                   ["dfile=", "mfile=", "time=", "z_postion="])
    except getopt.GetoptError:
        print('pyfoci.py -d <damagefile> -m <microscopefile> -t <viewtime> -z <z-slice>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('pyfoci.py -d <damagefile> -m <microscopefile> -t <viewtime> -z <z-slice>')
            sys.exit()
        elif opt in ("-d", "--dfile"):
            damage = arg
        elif opt in ("-m", "--mfile"):
            microscope_json = arg
        elif opt in ("-t", "--time"):
            time_to_view = int(arg)
        elif opt in ("-z", "--z_position"):
            z_position = int(arg)

    if len(opts) < 4:
        print('pyfoci.py -d <damagefile> -m <microscopefile> -t <viewtime> -z <z-slice>')
        sys.exit()

    print("Reading file.....")
    microscope_object = pyfoci.microscope.Microscope(damage, microscope_json, cell_radius=12)
    print("Defining Microscope.....")
    microscope_object.define_microscope()
    print("Mapping Breaks.....")
    microscope_object.mapping_breaks()
    print("Creating Microscope World.....")
    world = microscope_object.create_world()
    print("Placing Breaks.....")
    broken_world = microscope_object.place_breaks(world, time_to_view, z_stack_offset=z_position)
    print("Getting max value for normalisation.....")
    microscope_object.max_value_world()
    print("Making Images.....")
    view_object = pyfoci.view.View(microscope_object, broken_world)
    view_object.viewer()
    view_object.display_microscope_image()


if __name__ == "__main__":
    main(sys.argv[1:])
