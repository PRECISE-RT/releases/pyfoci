from math import sqrt
import sys

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.image import imread
from skimage.color import rgb2gray
from skimage.feature import blob_log
import os


class FociCounter(object):

    def __init__(self):
        self.microscope_image = None
        self.max_sigma = None
        self.min_sigma = None
        self.num_sigma = None
        self.threshold = None
        self.detected_array = None
        self.detections = None

    def reset_counter(self):
        self.__init__()

    def parse_input(self, microscope_image):
        if type(microscope_image) is str:
            image = imread(microscope_image)
            self.microscope_image = rgb2gray(image)
        elif type(microscope_image) is np.ndarray or np.matrix:
            self.microscope_image = microscope_image

    def detect_foci(self, min_sigma=1, max_sigma=30, num_sigma=10, threshold=0.08, threshold_relative=False, **kwargs):
        self.min_sigma = min_sigma
        self.max_sigma = max_sigma
        self.num_sigma = num_sigma
        self.threshold = threshold
        if threshold_relative:
            self.detected_array = blob_log(self.microscope_image,
                                    min_sigma=self.min_sigma,
                                    max_sigma=self.max_sigma,
                                    num_sigma=self.num_sigma,
                                    threshold_rel=self.threshold)
        else:
            self.detected_array = blob_log(self.microscope_image,
                                    min_sigma=self.min_sigma,
                                    max_sigma=self.max_sigma,
                                    num_sigma=self.num_sigma,
                                    threshold=self.threshold)

        self.detections = self.detected_array.shape[0]

    def plot_detected_array(self, vmax=None, figsize=(8,8), dpi=100):
        plt.clf()
        plt.figure(figsize=figsize, dpi=dpi)
        self.detected_array[:, 2] = self.detected_array[:, 2] * sqrt(2)
        if vmax!=None:
            plt.imshow(self.microscope_image, vmin=0, vmax=vmax)
        else:
            plt.imshow(self.microscope_image)
        fig = plt.gcf()
        ax = fig.gca()
        for detection in self.detected_array:
            y, x, r = detection
            c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
            ax.add_artist(c)
        plt.tight_layout()
        plt.axis('off')
        plt.show()

    def plot_detected_3D_array(self, vmax=None, figsize=(8,8), dpi=100):
        if self.detected_array.shape[1] < 4:
            print("Dataset must be 3-dimensional to call plot_detected_3D_array")
            sys.exit()
        self.detected_array[:, 3] = self.detected_array[:, 3] * sqrt(3)
        for slice in range(0,self.microscope_image.shape[0]):
            plt.clf()
            plt.figure(figsize=figsize, dpi=dpi)
            if vmax!=None:
                plt.imshow(self.microscope_image[slice,:,:], vmin=0, vmax=vmax)
            else:
                plt.imshow(self.microscope_image[slice,:,:])
            fig = plt.gcf()
            ax = fig.gca()
            for detection in self.detected_array:
                z, y, x, r = detection
                if slice<z+r and slice>z-r:
                    c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
                    ax.add_artist(c)
            plt.tight_layout()
            plt.axis('off')
            plt.show()

    def save_detected_array(self, output_name, vmax=None):
        self.detected_array[:, 2] = self.detected_array[:, 2] * sqrt(2)
        if vmax!=None:
            plt.imshow(self.microscope_image, vmin=0, vmax=vmax)
        else:
            plt.imshow(self.microscope_image)
        fig = plt.gcf()
        ax = fig.gca()
        for detection in self.detected_array:
            y, x, r = detection
            c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
            ax.add_artist(c)
        plt.tight_layout()
        plt.savefig(str(output_name), bbox_inches='tight')

    def save_detected_array_3d(self, output_name, vmax=None):
        if self.microscope_image.ndim < 3:
            print("Dataset must be 3-dimensional to call display_3d_microscope_image")
            sys.exit()
        self.detected_array[:, 3] = self.detected_array[:, 3] * sqrt(3)
        for i, slice in enumerate(range(0,self.microscope_image.shape[0])):
            if vmax!=None:
                plt.imshow(self.microscope_image[slice,:,:], vmin=0, vmax=vmax)
            else:
                plt.imshow(self.microscope_image[slice,:,:])
            fig = plt.gcf()
            ax = fig.gca()
            for detection in self.detected_array:
                z, y, x, r = detection
                if slice<z+r and slice>z-r:
                    c = plt.Circle((x, y), r, color='red', linewidth=2, fill=False)
                    ax.add_artist(c)
            plt.tight_layout()
            plt.axis('off')
            filename, extension = os.path.splitext(output_name)
            plt.savefig(f"{filename}_{i}.{extension}", bbox_inches='tight')
            plt.clf()



    class FociCounterOptimisation:

        def __init__(self):
            self.fc = FociCounter()
