import math
import matplotlib.pyplot as plt
import numpy as np
import sys
import os

class View(object):
    """
    This class handles the viewing of the microscope. The class requires a microscope object as an input.
    """

    def __init__(self, microscope_object, image_array):
        """
        :param microscope_object: object created by the Microscope class
        :param image_array: 2D/3D numpy array created by the place_breaks method in the Microscope class
        """
        self.microscope_object = microscope_object
        self.image_array = image_array
        self.view_adjusted_image = None

    def __repr__(self):
        return f'View({self.microscope_object}, {self.image_array})'

    def __len__(self):
        return len(self.image_array)

    def __getitem__(self, item):
        return self.image_array[item]

    def viewer(self):
        """
        :return: obj.view_adjusted_image - image_array param which has been modified for zoomed view of the cell
        """

        # viewpoint required pixels is for FOV not cell size,
        # just focus on cell size we pick inner box within created array
        fov = self.microscope_object.microscope_param['FOV']
        required_pixels_x = int((fov / (self.microscope_object.pixel_spacing_x * 1e6)))
        pixel_size = required_pixels_x / fov
        mid_point = math.ceil(required_pixels_x / 2)
        zoom_position = math.ceil((self.microscope_object.cell_radius + 0.5) * pixel_size)
        zoom_plus = mid_point + zoom_position
        zoom_negative = mid_point - zoom_position
        if self.image_array.ndim < 3:
            self.view_adjusted_image = self.image_array[zoom_negative:zoom_plus, zoom_negative:zoom_plus]
        else:
            self.view_adjusted_image = self.image_array[:,zoom_negative:zoom_plus, zoom_negative:zoom_plus]

    def display_microscope_image(self, details=False, self_normalisation=False, figsize=(8,8), dpi=100):
        """
        :param details: adds details regarding the microscope set-up to the outputted image
        :param self_normalisation: if True the outputted image will be normalised to self. If False it will be
        normalised to max_value of microscope object (see microscope.max_value_world() for more information).
        :return: matplotlib image displayed through plt.show()
        """
        plt.clf()
        plt.figure(figsize=figsize, dpi=dpi)
        if self_normalisation is True:
            self_normalised_image  = self.view_adjusted_image/self.microscope_object.max_value
            plt.imshow(self_normalised_image, interpolation='none')
        else:
            plt.imshow(self.view_adjusted_image, interpolation='none', vmin=0,
                       vmax=self.microscope_object.max_value)
        plt.axis('off')
        if details is True:
            # TODO Add a way for both time, z-stack, mag, microscope to be displayed on image by setting details=true
            # plt.text(20, 20, "Time After Exposure: 30s, z_pos = %s" % time_increment, fontsize=8, color='white')
            print("Feature still in Dev, run with details=False which is default")
            plt.show()
        else:
            plt.show()

    def display_3d_microscope_image(self, details=False, self_normalisation=False, figsize=(8,8), dpi=100):
        """
        :param details: adds details regarding the microscope set-up to the outputted image
        :param self_normalisation: if True the outputted image will be normalised to self. If False it will be
        normalised to max_value of microscope object (see microscope.max_value_world() for more information).
        :return: matplotlib image displayed through plt.show()
        """
        if self.view_adjusted_image.ndim < 3:
            print("Dataset must be 3-dimensional to call display_3d_microscope_image")
            sys.exit()
        for slice in range(0,self.view_adjusted_image.shape[0]):
            plt.clf()
            plt.figure(figsize=figsize, dpi=dpi)
            if self_normalisation is True:
                self_normalised_image  = self.view_adjusted_image/self.microscope_object.max_value
                plt.imshow(self_normalised_image[slice,:,:], interpolation='none')
            else:
                plt.imshow(self.view_adjusted_image[slice,:,:], interpolation='none', vmin=0,
                        vmax=self.microscope_object.max_value)
            plt.axis('off')
            if details is True:
                # TODO Add a way for both time, z-stack, mag, microscope to be displayed on image by setting details=true
                # plt.text(20, 20, "Time After Exposure: 30s, z_pos = %s" % time_increment, fontsize=8, color='white')
                print("Feature still in Dev, run with details=False which is default")
                plt.show()
            else:
                plt.show()

    def save_microscope_image(self, save_name, save_path='.', details=False, comment='', self_normalisation=False,
                              user_pad_inches=-0.4, dpi=200):
        """
        :param save_name: the filename you wish to save the image as. You must include the file extension.
        E.g. JPEG = #name#.jpg
        :param save_path: the directory path you wish to save the image in. Default is working directory.
        :param details: adds details regarding the microscope set-up to the outputted image
        :param comment: add comment to be printed on image. Only used if details=True
        :param self_normalisation: if True the outputted image will be normalised to self. If False it will be
        normalised to max_value of microscope object (see microscope.max_value_world() for more information).
        :param user_pad_inches: alters outside border from the image. Default is -0.4.
        :return: matplotlib image saved displayed through plt.savefig()
        """

        if self_normalisation is True:
            # self_normalised_image  = self.view_adjusted_image/self.microscope_object.max_value
            plt.imshow(self.view_adjusted_image, interpolation='none')
        else:
            plt.imshow(self.view_adjusted_image, interpolation='none', vmin=0,
                    vmax=self.microscope_object.max_value)
        plt.axis('off')
        if details is True:
            # TODO Add a way for both time, z-stack, mag, microscope to be displayed on image by setting details=true
            # plt.text(20, 20, "Time After Exposure: 30s, z_pos = %s" % time_increment, fontsize=8, color='white')
            print("Feature still in Dev, run with details=False which is default")
            plt.text(20, 20, comment, bbox=dict(facecolor='white', alpha=0.8))
            plt.savefig(f'{save_path}/{save_name}', bbox_inches='tight', pad_inches=user_pad_inches, dpi=dpi)
        else:
            plt.savefig(f'{save_path}/{save_name}', bbox_inches='tight', pad_inches=user_pad_inches, dpi=dpi)
        plt.clf()
        # print(f'The image {save_name} has been saved in {save_path}/')

    def save_microscope_image_3d(self, save_name, save_path='.', details=False, comment='', self_normalisation=False,
                            user_pad_inches=-0.4, dpi=200):
        """
        :param save_name: the filename you wish to save the image as. You must include the file extension.
        E.g. JPEG = #name#.jpg
        :param save_path: the directory path you wish to save the image in. Default is working directory.
        :param details: adds details regarding the microscope set-up to the outputted image
        :param comment: add comment to be printed on image. Only used if details=True
        :param self_normalisation: if True the outputted image will be normalised to self. If False it will be
        normalised to max_value of microscope object (see microscope.max_value_world() for more information).
        :param user_pad_inches: alters outside border from the image. Default is -0.4.
        :return: matplotlib image saved displayed through plt.savefig()
        """

        if self.view_adjusted_image.ndim < 3:
            print("Dataset must be 3-dimensional to call display_3d_microscope_image")
            sys.exit()
        for i, slice in enumerate(range(0,self.view_adjusted_image.shape[0])):
            if self_normalisation is True:
                # self_normalised_image  = self.view_adjusted_image/self.microscope_object.max_value
                plt.imshow(self.view_adjusted_image[slice,:,:], interpolation='none')
            else:
                plt.imshow(self.view_adjusted_image[slice,:,:], interpolation='none', vmin=0,
                        vmax=self.microscope_object.max_value)
            plt.axis('off')
            filename, extension = os.path.splitext(save_name)
            if details is True:
                # TODO Add a way for both time, z-stack, mag, microscope to be displayed on image by setting details=true
                # plt.text(20, 20, "Time After Exposure: 30s, z_pos = %s" % time_increment, fontsize=8, color='white')
                print("Feature still in Dev, run with details=False which is default")
                plt.text(20, 20, comment, bbox=dict(facecolor='white', alpha=0.8))
                plt.savefig(f'{save_path}/{filename}_{i}.{extension}', bbox_inches='tight', pad_inches=user_pad_inches, dpi=dpi)
            else:
                plt.savefig(f'{save_path}/{filename}_{i}.{extension}', bbox_inches='tight', pad_inches=user_pad_inches, dpi=dpi)
            plt.clf()
            # print(f'The image {save_name} has been saved in {save_path}/')
