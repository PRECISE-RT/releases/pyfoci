import copy
import json

import numpy as np
import pyqtgraph as pg
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QPushButton
from pyqtgraph.Qt import QtCore, QtGui

import pyfoci


class UserWindow(object):

    def __init__(self):
        super(UserWindow, self).__init__()
        # Run get_user_input on any call of the UserWindow class
        self.get_user_inputs()
        self.window = None
        self.ui = None

    def open_window(self, damage_file, select_microscope, select_magnification, sim_time, view_angle):
        # Create a new QMainWindow which is tied to the existence of UserWindow instance
        self.window = QtGui.QMainWindow()
        # Pass all needed user input to ViewWindow so the image can be generated
        self.ui = ViewWindow(damage_file, select_microscope, select_magnification, sim_time, view_angle)
        # Pass new QMainWindow to the gui_image_view method for the gui to be set out and populated
        self.ui.gui_image_view(self.window)
        # Generated image called within UserWindow to ensure it doesn't close once generated
        self.window.show()

    def get_user_inputs(self):
        # Define a top-level widget to hold everything
        w = QtGui.QWidget()
        w.setWindowTitle('pyfoci - user selection')

        # Labels for user inputs
        label_damage_file = QtGui.QLabel()
        label_damage_file.setText("Select Damage File: ")

        label_select_microscope = QtGui.QLabel()
        label_select_microscope.setText("Select Microscope: ")

        label_select_magnification = QtGui.QLabel()
        label_select_magnification.setText("Select Magnification: ")

        label_simulation_time = QtGui.QLabel()
        label_simulation_time.setText("Time After Irradiation (s): ")

        label_viewing_angle = QtGui.QLabel()
        label_viewing_angle.setText("Choose viewing angle (clockwise degrees): ")

        # Widgets for user input
        @pyqtSlot()
        def on_click_browse():
            dlg.exec_()
            self.damage_fp = dlg.selectedFiles()

        damage_file_btn = QPushButton('Browse')
        damage_file_btn.clicked.connect(on_click_browse)
        dlg = QtGui.QFileDialog()
        dlg.setFileMode(QtGui.QFileDialog.AnyFile)

        microscope_file = 'microscopes/guiMicroscopes.json'
        with open(microscope_file) as data_file:
            microscope_details = json.loads(data_file.read())

        select_microscope = QtGui.QComboBox()
        for microscope_name in microscope_details["PSF_file"]:
            select_microscope.addItem(microscope_name)
        # Function to dynamically change available magnifications based on what PSFs are available for chosen microscope

        def on_current_text_changed():
            select_magnification.clear()
            for microscope_name in microscope_details["PSF_file"]:
                if str(select_microscope.currentText()) == microscope_name:
                    for microscope_magnification in microscope_details["PSF_file"][microscope_name]:
                        select_magnification.addItem(microscope_magnification)

        select_magnification = QtGui.QComboBox()
        # Extremely ugly one-liner to get a initial set of values without explicitly using an additional for loop
        for microscope_magnification in microscope_details["PSF_file"][list(microscope_details["PSF_file"])[0]]:
            select_magnification.addItem(microscope_magnification)

        select_microscope.currentTextChanged.connect(on_current_text_changed)

        simulation_time = QtGui.QSpinBox()
        simulation_time.setRange(30, 86400)
        simulation_time.setSingleStep(30)

        viewing_angle = QtGui.QSpinBox()
        viewing_angle.setRange(0, 360)
        viewing_angle.setSingleStep(1)

        @pyqtSlot()
        def on_click_plot():
            print(self.damage_fp[0], str(select_microscope.currentText()), str(select_magnification.currentText()),
                  simulation_time.value(), viewing_angle.value())

            self.open_window(self.damage_fp[0], str(select_microscope.currentText()), str(select_magnification.currentText()),
                             simulation_time.value(), viewing_angle.value())

        plot_button = QPushButton('Plot')
        plot_button.setToolTip('Use Pyfoci to view the microscope')
        plot_button.clicked.connect(on_click_plot)

        # Create a grid layout to manage the widgets size and position
        layout = QtGui.QGridLayout()
        w.setLayout(layout)
        # Add widgets to the layout in their proper positions
        layout.addWidget(label_damage_file, 1, 0)  # button goes in upper-left, zeroth column
        layout.addWidget(damage_file_btn, 1, 1)  # button goes in upper-left, first column
        layout.addWidget(label_select_microscope, 2, 0)
        layout.addWidget(select_microscope, 2, 1)
        layout.addWidget(label_select_magnification, 3, 0)
        layout.addWidget(select_magnification, 3, 1)
        layout.addWidget(label_simulation_time, 4, 0)
        layout.addWidget(simulation_time, 4, 1)
        layout.addWidget(label_viewing_angle, 5, 0)
        layout.addWidget(viewing_angle, 5, 1)
        layout.addWidget(plot_button, 6, 0)

        # Display the widget as a new window
        w.show()

        # Start the Qt event loop
        app.exec_()


class ViewWindow(object):
    def __init__(self, damage_file, select_microscope, select_magnification, sim_time, view_angle):
        super(ViewWindow, self).__init__()
        self.damage_file = damage_file
        self.select_microscope = select_microscope
        self.select_magnification = select_magnification
        self.sim_time = sim_time
        self.view_angle = view_angle

    def get_pyfoci_image(self):
        image_list = []

        microscope_file = 'microscopes/guiMicroscopes.json'
        microscope_object = pyfoci.microscope.Microscope(self.damage_file, microscope_file)
        microscope_object.define_microscope_list(self.select_microscope,self.select_magnification)
        world = microscope_object.create_world()
        microscope_object.mapping_breaks(degrees_rotated=self.view_angle)

        for z_vals in range(-microscope_object.z_stack_limit, microscope_object.z_stack_limit):
            # Noticed that world gets changed every time it goes through place_breaks.
            scored_world = microscope_object.place_breaks(copy.copy(world), self.sim_time, z_stack_offset=z_vals)

            view_obj = pyfoci.view.View(microscope_object, scored_world)
            view_obj.viewer()

            image_list.append(copy.deepcopy(view_obj.view_adjusted_image))

        display_image = np.stack(image_list, axis=0)

        return display_image, microscope_object.z_stack_limit

    def gui_image_view(self, window):
        # Interpret image data as row-major instead of col-major
        pg.setConfigOptions(imageAxisOrder='row-major')

        # Utilise the passed through QMainWindow as window
        window.resize(1200, 800)
        imv = pg.ImageView()
        window.setCentralWidget(imv)
        window.setWindowTitle('Pyfoci')

        pyfoci_image, z_stack_limit = self.get_pyfoci_image()
        # Sanity check that the above image generation works correctly
        print("Image generated... OK")

        # Display the data and assign each frame to a z_stack slice
        imv.setImage(pyfoci_image, xvals=np.linspace(-z_stack_limit, z_stack_limit, pyfoci_image.shape[0]))

        # Set a custom color map - Green colour channel to emulate GFP
        colors = [
            (0, 0, 0),
            (0, 51, 0),
            (0, 102, 0),
            (0, 153, 0),
            (0, 204, 0),
            (0, 255, 0)
        ]
        cmap = pg.ColorMap(pos=np.linspace(0.0, 1.0, 6), color=colors)
        imv.setColorMap(cmap)


# Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        app = QtGui.QApplication([])
        UserWindow()
