import math

import numpy as np


class Parser(object):
    """
    This class should handle any file parsing into the microscope data structure of [time x y z] where each line is a
    DNA end. As more input files are allowed the readers should be added here.
    """

    def __init__(self, fp):
        """
        :param fp: string representation of the file path to the damage data
        """
        self.filePath = fp

    def read_csv(self, bin_size=30):
        """
        :param self:
        :param bin_size: the time bin size you want the csv file to be reformatted to
        :return: four numpy arrays [time x y z] where each line is a DNA end
        :raises keyError: key error
        """
        time_interval = bin_size
        line_number = 0
        interval_list = [0]
        sim_time = []
        x = []
        y = []
        z = []
        # Read in data
        with open(self.filePath) as f:
            for line in f:
                line = line.split(',')
                # Checking that number of breaks listed matches recorded breaks
                line_number += 1
                line_length = int(len(line))
                recorded_breaks = int(line[0])
                counted_breaks = (line_length - 2) / 6
                if int(recorded_breaks) != int(counted_breaks):
                    print('Mismatch between Recorded Breaks = %d and Counted Breaks = %d found on line = %d' %
                          (recorded_breaks, counted_breaks, line_number))
                recorded_time = float(line[1]) * 60  # Changing minutes to seconds
                needed_interval = math.floor(recorded_time / 30) - interval_list[-1]
                # Write output file
                for c in range(0, int(needed_interval)):
                    time_bin = time_interval * (c + interval_list[-1])
                    for i in range(0, int(counted_breaks) * 2):
                        sim_time.append(time_bin)
                        x.append(line[2 + (3 * i)])
                        y.append(line[3 + (3 * i)])
                        z.append(line[4 + (3 * i)])
                interval_list.append(interval_list[-1] + needed_interval)
        # Return as numpy arrays of float of efficient processing later and to mimic read_damaris method
        sim_time_np = np.array(sim_time).astype(np.float)
        x_np = np.array(x).astype(np.float)
        y_np = np.array(y).astype(np.float)
        z_np = np.array(z).astype(np.float)

        return sim_time_np, x_np, y_np, z_np

    def read_damaris(self):
        """
        :return: five numpy arrays [time x y z val] where each line is a DNA end. If val is not given it will be filled at unity (i.e. equal weighting).
        """
        try:
            sim_time, x, y, z, vals = np.loadtxt(self.filePath, delimiter=' ', unpack=True, usecols=(0,1,2,3,4))
            return sim_time, x, y, z, vals
        except ValueError:
            sim_time, x, y, z = np.loadtxt(self.filePath, delimiter=' ', unpack=True, usecols=(0,1,2,3))
            return sim_time, x, y, z, np.ones(sim_time.shape)

    def read_npy(self, diff_file=False):
        data = np.load(self.filePath)
        return data['time'], data['x'], data['y'], data['z'], data['val']

    # # TO COMPLETE FOR FUTURE REDUCTION IN H2AX FILE SIZE
    # def read_npy(self, diff_file=False):
    #     if diff_file is False:
    #         data = np.load(self.filePath)
    #         return data['time'], data['x'], data['y'], data['z'], data['val']
    #     else:
    #         dataset = np.load(self.filePath)
    #         data = np.copy(dataset)
    #         # 0 time point will be complete
    #         refmask = (dataset['time'] == 0)
    #         data['time'] = data['time'][refmask]
    #         data['x'] = data['x'][refmask]
    #         data['y'] = data['y'][refmask]
    #         data['z'] = data['z'][refmask]
    #         data['val'] = data['val'][refmask]

    #         for time in np.unique(dataset['time']):
    #             if time == 0:
    #                 continue

    #             mask = (dataset['time'] == time)
    #             inmask = ((np.isin(data['x'][refmask],dataset['x'][mask]))&(np.isin(data['y'][refmask],dataset['y'][mask]))&(np.isin(data['z'][refmask],dataset['z'][mask])))
    #             keepmask = np.invert(inmask)
    #             data['time'] = np.concatenate(data['time'],dataset['time'][keepmask])
    #             data['x'] = np.concatenate(data['x'],dataset['x'][keepmask])
    #             data['y'] = np.concatenate(data['y'],dataset['y'][keepmask])
    #             data['z'] = np.concatenate(data['z'],dataset['z'][keepmask])
    #             data['val'] = np.concatenate(data['val'],dataset['val'][keepmask])
