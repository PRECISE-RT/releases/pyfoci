import numpy as np
from numba import njit
import random
import math
from . import sddparser
import plotly.graph_objects as go

class BioProcessing():
    """
    This class handles various biological processing. 
    However, it's current main two roles include modelling bi-exponetial repair and modelling Histone activation to minic gamma-H2AX.
    """
    
    def __init__(self):

        self.bead_list = None
        self.damage_list = None
        self.nucleosome_size_bp = None
        self.histone_copies_per_nucleosome = None
        self.rng = None
        self.dist_to_bp = None
        self.time_store = []
        self.x_store = []
        self.y_store = []
        self.z_store = []
        self.val_store = []
        
    def read_bead_verticies_file(self,fp):
        """
        Function reads verticies and populates BioProcessing class.
        :param fp: Verticies file path as string. 
        """
        self.bead_list = []
        with open(fp, 'r') as f:
            data = f.readlines()
            for line in data:
                entry = {}
                if line.startswith('#'):
                    continue
                entry['chrom'] = line.split(' ')[0]
                entry['copy'] = line.split(' ')[1]
                entry['x'] = float(line.split(' ')[2])
                entry['y'] = float(line.split(' ')[3])
                entry['z'] = float(line.split(' ')[4])
                entry['radius'] = float(line.split(' ')[5])
                entry['bps'] = int(line.split(' ')[6].split('\n')[0])
                entry['damages'] = []
                entry['histones'] = []
                self.bead_list.append(entry)
        self.dist_to_bp = (entry['radius']*2)/entry['bps']
                
    def filter_beads_by_damage_only(self, damagefp, time, etol=0, skipbreaks=False):
        """
        Function removes TAD beads where no damage as occured to reduce compute time and file sizes.
        :param damagefp: Damage over time file path as string outputted from repair model. 
        :param time: Time at which to filter for damage only beads. 
        :param etol: Allowed tolerance between damage position and the TAD bead it belongs to.
        :param skipbreaks: Ignores the requirement of each break nees to correspond to TAD bead position.
        """
        sim_times, xs, ys, zs, _, hets, chroms, chromatids, _, chromposs, breakids = np.loadtxt(damagefp, delimiter=' ', unpack=True)
        filtered_bead_list = []
        i = 0
        n = 0
        for s_time, x, y, z, het, chrom, chromatid, chrompos, breakid in zip(sim_times, xs, ys, zs, hets, chroms, chromatids, chromposs, breakids):
            if s_time != time:
                continue
            i += 1
            for bead in self.bead_list:
                if ((x-bead['x']) ** 2) + ((y-bead['y']) ** 2) + ((z-bead['z']) ** 2) < (bead['radius']+etol)**2:
                    bead['damages'].append({'x':x, 'y':y, 'z':z, 'time':s_time, 'het_status':het, 'chromosome':chrom, 'chromatid':chromatid, 'chrompostion': chrompos, 'breakID': breakid})
                    if bead not in filtered_bead_list:
                        filtered_bead_list.append(bead)
                    # else:
                    #     print(bead)
                    n += 1
                    break
        # print(n,i)
        if skipbreaks is False:
            #print(n, i)
            assert n == i, "Not all damages have been associated to a bead. This can sometimes happen due to rounding between simulation softwares. Try increasing etol in this function. (e.g etol=1e-3)"
        self.bead_list = filtered_bead_list

    def reset_damages(self):
        for bead in self.bead_list:
            bead['damages'] = []

    def update_damage(self, damagefp, time, noMovement=True):
        """
        Function which updates the damages remaining in the simulation.
        :param damagefp: Damage over time file path as string outputted from repair model. 
        :param time: Time at which to filter for damage only beads. 
        :param noMovement: Assumes that the DNA damages are stationary, otherwise you need to track damage and bead position simultaneously (WIP).
        """
        sim_times, xs, ys, zs, _, hets, chroms, chromatids, _, chromposs, breakids = np.loadtxt(damagefp, delimiter=' ', unpack=True)
        filtered_bead_list = []
        mask = (sim_times == time)
        sim_times = sim_times[mask]
        xs = xs[mask]
        ys = ys[mask]
        zs = zs[mask]
        breakids = breakids[mask]      
        for bead in self.bead_list:
            for damage in bead['damages']:
                if damage['breakID'] not in breakids:
                    bead['damages'].remove(damage)
                    if len(bead['damages']) == 0:
                        self.bead_list.remove(bead)
                elif noMovement == False:
                    damage_mask = (breakids == damage['breakID'])
                    # Utilising the fact that True is equal to 1
                    assert sum(damage_mask)==1, 'Multiple copies of the same breakID at the same time point found. This should not of happened.'
                    damage['x'] = xs[damage_mask][0]
                    damage['y'] = ys[damage_mask][0]
                    damage['z'] = zs[damage_mask][0]
                    
    @staticmethod
    @njit
    def position_histones(required_histones, bead_x, bead_y, bead_z, bead_radius):
        """
        Function which randomly positions the histones within a single given TAD bead using the random seed.
        :param required_histones: Number of needed histones to place. 
        :param bead_x: X-position of the bead centre. 
        :param bead_y: Y-position of the bead centre. 
        :param bead_z: Z-position of the bead centre. 
        :param bead_radius: Size of bead radius.
        :return: Histone positions as a list of dictionaries.
        """
        histone_positions = []
        for histone in range(0, required_histones):
            positioned = False
            while positioned is False:
                x_pos = np.random.uniform(bead_x-bead_radius, bead_x+bead_radius)
                y_pos = np.random.uniform(bead_y-bead_radius, bead_y+bead_radius)
                z_pos = np.random.uniform(bead_z-bead_radius, bead_z+bead_radius)
                if ((x_pos-bead_x) ** 2) + ((y_pos-bead_y) ** 2) + ((z_pos-bead_z) ** 2) < (bead_radius)**2:
                    positioned = True
                    entry = {'x':x_pos, 'y':y_pos, 'z':z_pos, 'val':0}
                    histone_positions.append(entry)
        return histone_positions
                            
    def add_histones(self, seed=0, nucleosome_size_bp=146, histone_copies_per_nucleosome=2, fraction_h2ax=0.1, labelling_efficiency=1):
        """
        Function goes through all TAD beads needing histones and places them.
        :param seed: seed for random placement. Deafult is 0. 
        :param nucleosome_size_bp: Number of basepairs (bp) in nucleosome. Default is 146. 
        :param histone_copies_per_nucleosome: Number of H2A histones per nucleosome. Default is 2.  
        :param fraction_h2ax: Fraction of H2A histones which are the varient H2AX. Default is 0.1. 
        """
        self.rng = random.Random()
        self.rng.seed = seed
        self.nucleosome_size_bp = nucleosome_size_bp
        self.histone_copies_per_nucleosome = histone_copies_per_nucleosome
        for i, bead in enumerate(self.bead_list):
            # print(f'working on bead ({i+1}/{len(self.bead_list)+1}): {bead["chrom"]}-{bead["copy"]}-{bead["bps"]}\n')
            required_histones = math.ceil((bead['bps']/(self.nucleosome_size_bp/self.histone_copies_per_nucleosome))*fraction_h2ax*labelling_efficiency)
            np.random.seed(self.rng.randint(1,1e6))
            positioned_histones = self.position_histones(required_histones, bead['x'], bead['y'], bead['z'], bead['radius'])
            bead['histones'] = positioned_histones
    
    @staticmethod
    def get_cauchy_contribution(bp_from_dsb, A, H, FWHM):
        """
        Function calculates the histone activation based on distance in bp from the DSB to histone. Default values are derrived from a Cauchy fit to the Arnould et al., 2020 ChIP-Seq Data Fig 2b data.
        :param bp_from_dsb: Distance in bp from the DSB to histone. Note: This is convereted to Mbp for fit parameters. 
        :param A: Cauchy distribution fit parameter A. 
        :param H: Cauchy distribution fit parameter H. 
        :param FWHM: Cauchy distribution fit parameter FWHM. 
        :return: Cauchy distribution activation value.
        """
        # chat-studio plotly fit of the Arnould et al., 2020 ChIP-Seq Data Fig 2b
        mbp_from_dsb = bp_from_dsb*1e-6
        return A + H/(1+4*((mbp_from_dsb)/FWHM)**2)
                            
    def compute_histone_activation(self, reset_vals=False, A=0.22568542777797262, H=0.3848433186715101, FWHM=0.4488695944536109):
        """
        Function loops through beads and damages within the bead to calculate the total activations for a given histone.
        :param reset_vals: resets the value of activation before calculating new activation.
        """
        for bead in self.bead_list:
            for i, damage in enumerate(bead['damages']):
                for histone in bead['histones']:
                    if i == 0 and reset_vals == True:
                        histone['val'] = 0
                    bead_histone_distance = ((damage['x']-histone['x'])**2 + (damage['y']-histone['y'])**2 + (damage['z']-histone['z'])**2)**0.5
                    bead_histone_effective_bp_distance = bead_histone_distance/self.dist_to_bp
                    histone['val'] += self.get_cauchy_contribution(bead_histone_effective_bp_distance, A, H, FWHM)

    def store_data(self, time):
        """
        Add to data store for easy saving.
        :param time: time point at which to add data.
        """
        for bead in self.bead_list:
            for histone in bead['histones']:
                self.time_store.append(time)
                self.x_store.append(histone["x"])
                self.y_store.append(histone["y"])
                self.z_store.append(histone["z"])
                self.val_store.append(histone["val"])

    def save_as_npy(self, save_name='MicroscopeSnapshotH2AX', compressed=True):
        """
        Function saves data as compressed .NPY file. Recommended for histone data due to large data size.
        :param save_name: give a save name to the outputted file. Deafult is MicroscopeSnapshotH2AX.
        :param compressed: choose to compress the .NPY file saving more space. Default is True.
        """
        assert len(self.time_store) == len(self.x_store) == len(self.y_store) == len(self.z_store) == len(self.val_store), "Not all data store lengths match. This will cause issues later!"
        # data = np.array([self.time_store, self.x_store, self.y_store, self.z_store, self.val_store])
        if compressed:
            np.savez_compressed(f'{save_name}.npz', time=np.array(self.time_store, dtype=np.uint32), x=np.array(self.x_store, dtype=np.float32),
                                y=np.array(self.y_store, dtype=np.float32), z=np.array(self.z_store, dtype=np.float32), val=np.array(self.val_store, dtype=np.float32))
        else:
            np.save('{save_name}.npy', time=np.array(self.time_store, dtype=np.uint32), x=np.array(self.x_store, dtype=np.float32),
                                y=np.array(self.y_store, dtype=np.float32), z=np.array(self.z_store, dtype=np.float32), val=np.array(self.val_store, dtype=np.float32))

    def write_h2ax_file(self, time, save_name='MicroscopeSnapshotH2AX.out', append=False):
        """
        Function saves data as text file. Not recommended for histone data due to large data size.
        :param time: give a designated time point for saving.
        :param save_name: give a save name to the outputted file. Deafult is MicroscopeSnapshotH2AX.out.
        :param append: append data for the text file.
        """
        if append:
            file = open(save_name, 'a')
        else:
            file = open(save_name, 'w+')
        for bead in self.bead_list:
            for histone in bead['histones']:
                file.write(f'{time} {histone["x"]} {histone["y"]} {histone["z"]} {histone["val"]}\n')

    @staticmethod
    def write_microscope_file_from_sdd(sdd_fp, damage_reapeat=0, save_name='MicroscopeSnapshot1SDDGen.out'):
        """
        Function that writes the microscope files directly from the SDD damage file format.
        :param sdd_fp: File path to SDD file as string.
        :param damage_reapeat: If SDD file has multiple repeats which repeat do you want to write microcope file for.
        :param save_name: Save name of the generated microcope file.
        """
        sdd = sddparser.parseSDDFile(sdd_fp)
        chrom_sizes = sdd[0]['Chromosomes'][1]
        file = open(save_name, 'w+')
        i = 0
        for event in sdd[1][damage_reapeat]:
            # Write twice for each break end
            if event["Chromosome ID"][1] > 23:
                position_along = int(event["Chromosome Position"])/float(chrom_sizes[event["Chromosome ID"][1]-23])
            else:
                position_along = int(event["Chromosome Position"])/float(chrom_sizes[event["Chromosome ID"][1]])
            file.write(f'0 {event["Pos"][0][0]} {event["Pos"][0][1]} {event["Pos"][0][2]} 1.0 {event["Chromosome ID"][0]} {event["Chromosome ID"][1]} {event["Chromosome ID"][2]} {event["Chromosome ID"][3]} {position_along} {i}\n')
            i += 1
            file.write(f'0 {event["Pos"][0][0]} {event["Pos"][0][1]} {event["Pos"][0][2]} 1.0 {event["Chromosome ID"][0]} {event["Chromosome ID"][1]} {event["Chromosome ID"][2]} {event["Chromosome ID"][3]} {position_along} {i}\n')
            i += 1
        file.close()

    @staticmethod
    def bi_exp_repair_sim(sdd_fp, damage_reapeat=0, save_name='MicroscopeSnapshotBiRep1.out', a1=0.711, a2=0.289, t1=1.54, t2=10, time_arr=None):
        """
        Make a Microscope file with included bi-exponential repair kinetics of DSB. 
        :param sdd_fp: File path to SDD file as string.
        :param damage_reapeat: If SDD file has multiple repeats which repeat do you want to write microcope file for.
        :param save_name: Save name of the generated microcope file.
        :param a1: a1 coefficient of the bi-expoential equation.
        :param a2: a2 coefficient of the bi-expoential equation.
        :param t1: tau1 coefficient of the bi-expoential equation.
        :param t2: tau2 coefficient of the bi-expoential equation.
        :param time_arr: The different repair times you want to store results for.
        """
        if time_arr is None:
            time_arr = [0,900,1800,3600,7200,10800,14400,18000,21600,25200,28800,32400,36000,39600,43200,46800,50400,54000,57600,61200,64800,68400,72000,75600,79200,82800,86400]
        results = {time:[] for time in time_arr}
        sdd = sddparser.parseSDDFile(sdd_fp)
        chrom_sizes = sdd[0]['Chromosomes'][1]
        i = 0
        for event in sdd[1][damage_reapeat]:
            # Write twice for each break end
            if event["Chromosome ID"][1] > 23:
                position_along = int(event["Chromosome Position"])/float(chrom_sizes[event["Chromosome ID"][1]-23])
            else:
                position_along = int(event["Chromosome Position"])/float(chrom_sizes[event["Chromosome ID"][1]])
            results[0].append(f'0 {event["Pos"][0][0]} {event["Pos"][0][1]} {event["Pos"][0][2]} 1.0 {event["Chromosome ID"][0]} {event["Chromosome ID"][1]} {event["Chromosome ID"][2]} {event["Chromosome ID"][3]} {position_along} {i}\n')
            i += 1
            results[0].append(f'0 {event["Pos"][0][0]} {event["Pos"][0][1]} {event["Pos"][0][2]} 1.0 {event["Chromosome ID"][0]} {event["Chromosome ID"][1]} {event["Chromosome ID"][2]} {event["Chromosome ID"][3]} {position_along} {i}\n')
            i += 1
        total_break_ends = len(results[0])
        prev_time = 0
        for time in time_arr:
            if time == 0:
                continue
            new_break_ends = math.ceil(total_break_ends * (a1*math.exp(-(time/3600)/t1) + a2*math.exp(-(time/3600)/t2)))
            chosen = random.sample(results[prev_time], new_break_ends)
            chosen = [f'{time}{entry[len(str(prev_time)):]}' for entry in chosen]
            results[time] = chosen
            prev_time = time

        file = open(save_name, 'w+')
        for time in results:
            for event in results[time]:
                file.write(event)
        file.close()


    @staticmethod
    def plot_activiation(bead, opacity=0.5, colorscale='BuGn', save_fig=False, save_name='HistoneActivation.html'):
        """
        Util function to make a 3D plot of bead activation of histones for a single bead. 
        :param bead: Bead object from bead_list.
        :param opacity: Opactity of the histones alter to help visualisation.
        :param colorscale: Colorscale to use for intensity. Deafult is blue to green.
        :param save_fig: Chosse to save figure as html. Default is False.
        :param save_name: If firgure it saved. Choose a save name. Default is HistoneActivation.html.
        """
        fig = go.Figure()
        xs = []
        ys = []
        zs = []
        vals = []
        for entry in bead['histones']:
            xs.append(entry['x'])
            ys.append(entry['y'])
            zs.append(entry['z'])
            vals.append(entry['val'])
        fig.add_trace(go.Scatter3d(x=xs, y=ys, z=zs,
                            mode='markers',
                            marker=dict(
                                size=4,
                                color=vals,
                                colorscale=colorscale,
                                opacity=opacity
                            )))
        for damage in bead['damages']:
            fig.add_trace(go.Scatter3d(x=[damage['x']], y=[damage['y']], z=[damage['z']],
                                mode='markers',
                                marker=dict(
                                    symbol='x',
                                    size=5,
                                    color='rgba(207, 0, 15, 1)',
                                    opacity=1
                                )))
        fig.update_layout(margin=dict(l=0, r=0, b=0, t=0))
        fig.show()
        if save_fig:
            fig.write_html(save_name)
        
