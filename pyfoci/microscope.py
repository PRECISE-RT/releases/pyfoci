import json
import math
import copy
import sys

import numpy as np
import h5py

# local imports
from .parser import Parser


class Microscope(object):
    """
    This class handles the representation of the DNA DSB ends in a simulated microscope. DNA DSB ends are placed in
    simulated space which is altered by the Microscope parameters. The Microscope parameters should be located in the
    microscope/*name*.json file, where *name* is the name of the microscope you wish to use.
    """

    def __init__(self, damage_file, microscope_param, cell_radius=2.5):
        """
        :param damage_file: string representation of the file path to the damage data
        :param microscope_param: microscope.json file which provides details on microscope to emulate
        :param cell_radius: radius of the simulated cell the damage data is based on
        """
        # Paths of inputted files
        self.damage_path = damage_file
        self.microscope_path = microscope_param
        # Load in the DNA DSB end damage file
        if damage_file.endswith('.csv'):
            self.sim_time, self.x, self.y, self.z = Parser(damage_file).read_csv()
        elif damage_file.endswith('.out'):
            self.sim_time, self.x, self.y, self.z, self.vals = Parser(damage_file).read_damaris()
        elif damage_file.endswith('.npz'):
            self.sim_time, self.x, self.y, self.z, self.vals = Parser(damage_file).read_npy()
        else:
            print("The damage_file argument: %s is not in usual .csv or .out format." % damage_file)
            exit()

        # Load in the microscope parameter file
        with open(microscope_param) as data_file:
            data = json.loads(data_file.read())
        self.microscope_param = data

        # Set-up cell radius used in simulation
        self.cell_radius = cell_radius
        # Set-up variables to be filled when defining the microscope
        self.pixel_spacing_y = None
        self.pixel_spacing_x = None
        self.pixel_spacing_z = None
        self.psf_matrix = None
        self.max_value = None
        self.z_stack_offset = 0
        self.z_stack_limit = None
        self.breaks_in_slice = None

    def __repr__(self):
        return f'Microscope({self.damage_path}, {self.microscope_path}, cell_radius={self.cell_radius})'

    def __len__(self):
        return [len(self.sim_time), len(self.x), len(self.y), len(self.z)]

    def __getitem__(self, item):
        return [self.sim_time[item], self.x[item], self.y[item], self.z[item]]

    def define_microscope(self):
        """
        :return: microscope object PSF details of the emulated microscope extracted from a .h5 file specified in the
        microscope json file
        """
        psf = self.microscope_param['PSF_file']
        f = h5py.File(psf, 'r')
        a_group_key = list(f.keys())[0]
        image_data = np.array(list(f[a_group_key]['ImageData']['Image']), dtype=np.float32)
        self.psf_matrix = image_data[0][0]
        self.pixel_spacing_y = f[a_group_key]['ImageData']['DimensionScaleY'][()]
        self.pixel_spacing_x = f[a_group_key]['ImageData']['DimensionScaleX'][()]
        self.pixel_spacing_z = f[a_group_key]['ImageData']['DimensionScaleZ'][()]
        self.z_stack_limit = math.ceil((self.cell_radius+0.5) / (self.pixel_spacing_z * 1e6))

        return self

    def define_microscope_list(self, microscope_name, magnification):
        """
        To use if you are loading a microscope.json with multiple PSF files in it - used in gui.py
        :return: microscope object PSF details of the emulated microscope extracted from a .h5 file specified in the
        microscope json file
        """
        psf = self.microscope_param['PSF_file'][microscope_name][magnification]
        f = h5py.File(psf, 'r')
        a_group_key = list(f.keys())[0]
        image_data = np.array(list(f[a_group_key]['ImageData']['Image']))
        self.psf_matrix = image_data[0][0]
        self.pixel_spacing_y = f[a_group_key]['ImageData']['DimensionScaleY'][()]
        self.pixel_spacing_x = f[a_group_key]['ImageData']['DimensionScaleX'][()]
        self.pixel_spacing_z = f[a_group_key]['ImageData']['DimensionScaleZ'][()]
        self.z_stack_limit = math.ceil((self.cell_radius+0.5) / (self.pixel_spacing_z * 1e6))

        return self

    @staticmethod
    def rotation_matrix(axis, theta):
        """
        Based on the Euler–Rodrigues formula
        :param axis: Default [1,0,0] which rotates yz along the x-axis
        :param theta: rotation angle
        :return: the rotation matrix associated with counterclockwise rotation about the given axis by theta radians.
        """
        axis = np.asarray(axis)
        axis = axis / math.sqrt(np.dot(axis, axis))
        a = math.cos(theta / 2.0)
        b, c, d = -axis * math.sin(theta / 2.0)
        aa, bb, cc, dd = a * a, b * b, c * c, d * d
        bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
        return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                         [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                         [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])

    def mapping_breaks(self, degrees_rotated=0, rotation_axis=None):
        """
        :param degrees_rotated: counterclockwise rotation of view plane in degrees, where 0 is default and is line with the beam direction
        :param rotation_axis: assigned axis of rotation following right-hand rule, default [1,0,0] which rotates yz along x-axis
        :return: microscope object with break co-ordinates remapped to centre of the imaging simulation world
        """
        # Only rotate if required
        if degrees_rotated != 0 or 360:
            rotated_damage = []
            # convert degrees to radians for rotation_matrix calculation
            rotation_radians = math.radians(degrees_rotated)
            if rotation_axis is None:
                rotation_axis = [1, 0, 0]
            rot_matrix = self.rotation_matrix(rotation_axis, rotation_radians)
            for x, y, z in zip(self.x, self.y, self.z):
                vector_to_rotate = [x, y, z]
                rotated_damage.append(np.dot(rot_matrix, vector_to_rotate))
            # round values of rotated_damage array (E-R formula has tiny ~e-16 offsets at cardinal angles)
            rotated_damage = np.around(rotated_damage, decimals=5)
            # replace the stored positions of the damage with the new rotated positions ready for aligning below
            self.x = [i[0] for i in rotated_damage]
            self.y = [i[1] for i in rotated_damage]
            self.z = [i[2] for i in rotated_damage]

        fov = self.microscope_param['FOV']
        x_shift = int((fov / (self.pixel_spacing_x * 1e6)))
        y_shift = int((fov / (self.pixel_spacing_y * 1e6)))

        # Align the damage (0,0,0) to the world centre and voxelise breaks
        self.x = [math.ceil(int(i / (self.pixel_spacing_x * 1e6)) + (x_shift/2)) for i in self.x]
        self.y = [math.ceil(int(i / (self.pixel_spacing_y * 1e6)) + (y_shift/2)) for i in self.y]
        self.z = [math.ceil(int(i / (self.pixel_spacing_z * 1e6))) for i in self.z]

        return self

    def create_world(self):
        """
        :return: 2D numpy array at the correct size for population of breaks
        """
        # Multiplication by 1e6 corrects for cell radius in micrometre and PSF in metre
        fov = self.microscope_param['FOV']
        required_pixels_x = int((fov / (self.pixel_spacing_x * 1e6)))
        required_pixels_y = int((fov / (self.pixel_spacing_y * 1e6)))
        simulation_world = np.zeros((required_pixels_x, required_pixels_y), dtype=np.float32)

        return simulation_world

    def max_value_world(self, provided_world=None, set_time_point=False, set_normalisation_value=False):
        """
        :param set_time_point: If not False, user give a time point for normalisation during viewing (e.g. set_time_point= 60)
        :param set_normalisation_value: if not False, user can set a normalisation value (e.g. set_normalisation_value = 1.8)
        :return: microscope object records the maximum intensity from earliest time point for normalisation of image
        """
        if provided_world is None:
            fov = self.microscope_param['FOV']
            required_pixels_x = int((fov / (self.pixel_spacing_x * 1e6)))
            required_pixels_y = int((fov / (self.pixel_spacing_y * 1e6)))
            temp_world = np.zeros((required_pixels_x, required_pixels_y))
            # Setting max value for normalisation
            if set_normalisation_value is not False:
                self.max_value = set_normalisation_value
                return self

            if set_time_point is not False:
                norm_sim_time = set_time_point
            else:
                norm_sim_time = min(self.sim_time)

            for sim_time, x, y, z , val in zip(self.sim_time, self.x, self.y, self.z, self.vals):

                if sim_time > norm_sim_time:
                    break

                if sim_time == norm_sim_time:
                    y_length = int(self.psf_matrix.shape[1]/2)
                    x_length = int(self.psf_matrix.shape[2]/2)
                    z_offset = int(self.psf_matrix.shape[0]/2)
                    z_contribution = -z + z_offset + self.z_stack_offset
                    try:
                        temp_world[-x_length+x:x_length+x, -y_length+y:y_length+y] += self.psf_matrix[z_contribution]*val
                    except IndexError:
                        continue          

            self.max_value = np.amax(temp_world)
        else:
            self.max_value = np.amax(provided_world)

        return self

    def place_breaks(self, simulation_world, set_time, z_stack_offset=0, deconvolve=False):
        """
        :param simulation_world: 2D/3D numpy array produced from the create_world method
        :param set_time: the time after irradiation (allowing for repair) you want to visualise the damage
        :param z_stack_offset: the position of the image slice, where z=0 is the centre of the cell. If a tuple is given a 3D stack will be taken.
        :return: 2D numpy array which has been populated by the intensity fluorescence of the breaks -
        ready for View class
        """
        self.z_stack_offset = z_stack_offset
        breaks_in_slice_count = 0
        breaks_in_cell = 0
        if type(z_stack_offset) is int:
            scored_simulation_world = copy.deepcopy(simulation_world)
        elif type(z_stack_offset) is tuple:
            number_of_stacks = abs(z_stack_offset[0]-z_stack_offset[1])+1
            scored_simulation_world = np.stack([copy.deepcopy(simulation_world) for _ in range(0,number_of_stacks)],axis=0)
        else:
            print("z_stack_offset should be an int if 2D or tuple if 3D analysis")
            sys.exit()
        # Place breaks for the set_time given to the function
        if type(z_stack_offset) is int:
            for sim_time, x, y, z, val in zip(self.sim_time, self.x, self.y, self.z, self.vals):

                if sim_time == set_time:
                    breaks_in_cell += 1
                    # count number of physical breaks actually in microscope slice
                    if z == z_stack_offset:
                        breaks_in_slice_count += 1
                    if deconvolve:
                        if z ==  z_stack_offset:
                            scored_simulation_world[x, y] += val
                    else:
                        # print(simulation_world.shape)
                        y_length = int(self.psf_matrix.shape[1]/2)
                        x_length = int(self.psf_matrix.shape[2]/2)
                        z_offset = int(self.psf_matrix.shape[0]/2)
                        z_contribution = -z + z_offset + self.z_stack_offset
                        try:
                            scored_simulation_world[-x_length+x:x_length+x, -y_length+y:y_length+y] += self.psf_matrix[z_contribution]*val
                        except IndexError:
                            continue
        else: # must be 3D
            for z_score, z_stack in enumerate(range(z_stack_offset[0],z_stack_offset[1]+1)):
                for sim_time, x, y, z, val in zip(self.sim_time, self.x, self.y, self.z, self.vals):
                    if sim_time == set_time:
                        if z_score==0:
                            breaks_in_cell += 1
                        # count number of physical breaks actually in microscope slice
                        if z == z_stack:
                            breaks_in_slice_count += 1
                        if deconvolve:
                            if z ==  z_stack:
                                scored_simulation_world[z_score, x, y] += val
                        else:
                            if z ==  z_stack:
                                y_length = int(self.psf_matrix.shape[1]/2)
                                x_length = int(self.psf_matrix.shape[2]/2)
                                z_length = math.floor(self.psf_matrix.shape[0]/2)
                                z_score_hl = math.floor(number_of_stacks/2)
                                z_short = False
                                z_long = False
                                if z-z_length<(-z_score_hl):
                                    z_short = True
                                if z+z_length>(z_score_hl):
                                    z_long = True
                                if z_short and z_long:
                                    try:
                                        scored_simulation_world[:, -x_length+x:x_length+x, -y_length+y:y_length+y] += self.psf_matrix[-z_score_hl+z+z_length:z_score_hl+z+z_length+1]*val
                                    except IndexError:
                                        continue
                                elif z_short and not z_long:
                                    try:
                                        scored_simulation_world[:, -x_length+x:x_length+x, -y_length+y:y_length+y] += self.psf_matrix[-z_score_hl+z+z_length:]*val
                                    except IndexError:
                                        continue
                                elif not z_short and z_long:
                                    try:
                                        scored_simulation_world[:, -x_length+x:x_length+x, -y_length+y:y_length+y] += self.psf_matrix[:z_score_hl+z+z_length]*val
                                    except IndexError:
                                        continue
                                else:
                                    try:
                                        scored_simulation_world[-z_length+z+z_score_hl:z_length+z+z_score_hl, -x_length+x:x_length+x, -y_length+y:y_length+y] += self.psf_matrix*val
                                    except IndexError:
                                        continue
        self.breaks_in_slice = breaks_in_slice_count
        self.breaks_in_cell = breaks_in_cell
        return scored_simulation_world
