import pyfoci
import sys
import getopt


def main(argv):

    damage, microscope_json, time_to_view, z_position, save_name, output_dir, view_angle, user_output_dir = \
        "", "", 0, 0, "", "", 0, False

    try:
        opts, args = getopt.getopt(argv, "hovd:m:t:z:s:",
                                   ["dfile=", "mfile=", "time=", "z_postion=", "save_name=", "ofile="])
    except getopt.GetoptError:
        print('pyfoci.py -d <damagefile> -m <microscopefile> -t <viewtime> -z <z-slice> -s <save_name> '
              '\nOPTIONAL FLAGS: -v <view_angle> -o <save_directory>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('pyfoci.py -d <damagefile> -m <microscopefile> -t <viewtime> -z <z-slice> -s <save_name> '
                  '\nOPTIONAL FLAGS: -v <view_angle> -o <save_directory>')
            sys.exit()
        elif opt in ("-d", "--dfile"):
            damage = arg
        elif opt in ("-m", "--mfile"):
            microscope_json = arg
        elif opt in ("-t", "--time"):
            time_to_view = int(arg)
        elif opt in ("-z", "--z_position"):
            z_position = int(arg)
        elif opt in ("-s", "--save_name"):
            save_name = arg
        elif opt in ("-v", "--view_angle"):
            z_position = int(arg)
        elif opt in ("-o", "--ofile"):
            output_dir = args[0]
            user_output_dir = True

    if len(opts) < 5:
        print('pyfoci.py -d <damagefile> -m <microscopefile> -t <viewtime> -z <z-slice> -s <save_name> '
              '\nOPTIONAL FLAGS: -v <view_angle> OPTIONAL FLAGS: -o <save_directory>')
        sys.exit()

    print("Reading file.....")
    microscope_object = pyfoci.microscope.Microscope(damage, microscope_json, cell_radius=12)
    print("Defining Microscope.....")
    microscope_object.define_microscope()
    print("Mapping Breaks.....")
    microscope_object.mapping_breaks(degrees_rotated=view_angle)
    print("Creating Microscope World.....")
    world = microscope_object.create_world()
    print("Placing Breaks.....")
    broken_world = microscope_object.place_breaks(world, time_to_view, z_stack_offset=z_position)
    print("Getting max value for normalisation.....")
    microscope_object.max_value_world()
    print("Making Images.....")
    view_object = pyfoci.view.View(microscope_object, broken_world)
    view_object.viewer()
    if user_output_dir is True:
        view_object.save_microscope_image(save_name, save_path=output_dir)
    else:
        view_object.save_microscope_image(save_name)


if __name__ == "__main__":
    main(sys.argv[1:])
