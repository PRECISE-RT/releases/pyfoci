import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pyfoci",
    version = "1.0",
    author = "Samuel Ingram",
    author_email = "samuel.ingram1@nhs.net",
    description = ("A computational approach to visualising and evaluating radiation-induced fluorescent foci with Python."),
    license = "GNU General Public License v3.0",
    keywords = "",
    packages=['pyfoci'],
    long_description=read('README'),
)
